 (function () {
    'use strict';

    angular.module('BlurAdmin.pages.usuario')
        .controller('mapsCtrl', mapsCtrl);

    /** @ngInject */
    function mapsCtrl($scope, $filter, $rootScope, NgMap) {
        console.log('entrou aqui mapsCtrl');
        $(".btn-close-map").click(function(){
            console.log("clicouuuuu");
            $(".ng-map-info-window").hide();
            $("#map").hide();
        });

        NgMap.getMap().then(function(map) {
            $rootScope.map = map;
            console.log("$rootScope.map", $rootScope.map);
        });
    } 

})();