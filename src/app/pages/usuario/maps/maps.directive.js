/**
* @author v.lugovksy
* created on 16.12.2015
*/
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.usuario')
        .directive('maps', maps);

    /** @ngInject */
    function maps() {
        return {
            restrict: 'AE',
            controller: 'mapsCtrl',
            templateUrl: 'app/pages/usuario/maps/maps.html'
        };
    }
})();
