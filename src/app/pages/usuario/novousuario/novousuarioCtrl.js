(function () {
 // 'use strict';

  angular.module('BlurAdmin.pages.templos')
      .controller('novousuarioCtrl', novousuarioCtrl);

	/** @ngInject */
	function novousuarioCtrl($scope) {
		var vm = this;
		vm.switches = {
	      btnStatus : true,
	    };

		//Estados
		$scope.estados = ["AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"],
		
		//Objeto Aninhada Semana
		$scope.itemSemana = [
		  	{"title" : "Domingo", "name" : "domingo", "active" : false, "start": "00:00","finish" : "00:00"},
		  	{"title" : "Segunda", "name" : "segunda", "active" : false, "start": "00:00","finish" : "00:00"},
		  	{"title" : "Terça",   "name" : "terca",   "active" : false, "start": "00:00","finish" : "00:00"},
		  	{"title" : "Quarta",  "name" : "quarta",  "active" : false, "start": "00:00","finish" : "00:00"},
		  	{"title" : "Quinta",  "name" : "quinta",  "active" : false, "start": "00:00","finish" : "00:00"},
		  	{"title" : "Sexta",   "name" : "sexta",   "active" : false, "start": "00:00","finish" : "00:00"},
		  	{"title" : "Sábado",  "name" : "sabado",  "active" : false, "start": "00:00","finish" : "00:00"},
		];

		//Array de models do formulario de registro de novo templo.
		$scope.form = {
			temple_name : "",
			temple_neighborhood : "",
			temple_address : "",
			temple_number : "",
			temple_city : "",
			temple_uf : "",
			lat : "",
			lon : "",
			created_at : "",
			status : "",
			// week : {
			// 	day_week : "",
			// 	start : "",
			// 	finish : ""
			// }
		};

		$scope.validateRules = {
			temple_name : { presence: { message: "Informe o nome", allowEmpty : false } },
			temple_address : { presence: { message: "Informe o endereço", allowEmpty : false  } },
			temple_number : { presence: { message: "Informe o numero", allowEmpty : false } },
			temple_neighborhood : { presence: { message: "Informe o bairro", allowEmpty : false } },
			temple_uf : { presence: { message: "Informe o estado", allowEmpty : false } },	
			week: { timeValid : { message: " Informe ao menos um horario de funcionamento" } },

			// start : { presence: { message: "Informe horario de inicio", allowEmpty : false } },
			// finish : { presence: { message: "Informe horario de Termino", allowEmpty : false } },

			temple_city : { 
				presence: { 
					message: "Informe o cidade", 
					allowEmpty : false 
				}
			},

			//latitude : { presence: { message: "Informe o latitude", allowEmpty : false } },
			//longitude : { presence: { message: "Informe o longitude", allowEmpty : false } },
		};
		//value  = valor do campo
		//options parametros
		//key = Chave do obejeto
		$scope.validTime = function(value, options, key, attributes) {
		  if(Array.isArray(value) && value.length > 0){
		  	for(var i in value ){
				var row = value[i];
				console.log(row);
				if(['segunda','terca','quarta','quinta','sexta','sabado','domingo'].indexOf(row.day_week) == -1){
					console.log("Dia da semana inválido");
					return "Dia da semana inválido";
				}
				if(row.start == "" || row.finish == ""){
					console.log("Preencha o horario de funcionamento");
					return "Preencha o horario de funcionamento";
				}
				console.log(Date.parse('2018-01-01 ' + row.start), Date.parse('2018-01-01 ' + row.finish));
				if(Date.parse('2018-01-01 ' + row.start) >= Date.parse('2018-01-01 ' + row.finish)){
					console.log("Horario de inicio tem que ser menor que horario de fim");
					return "Horario de Inicio tem que ser menor que horario de Termino";	
				}

		  	}
		  	return null;
		  }else{
		  	return options.message;
		  }
		};

		validate.validators.timeValid = $scope.validTime; 


		// $scope.setSemana = function(){
		// 	var selecionou = false;
		// 	var diasSelecionados = [];

		// 	for(field in $scope.itemSemana){
		// 		if ($scope.itemSemana[field]["active"]) {
		// 			selecionou = true;
		// 			diasSelecionados.push($scope.itemSemana[field]["name"]);
		// 		}
		// 	}
		// 	if(selecionou){
		// 		alert("você selecionou: " + diasSelecionados)
		// 	}else{	
		// 		alert("selecione um dia de culto");
		// 	}
		// };

		$scope.onSubmit = function(){
			
			console.log("scope.onSubmit");

			//BTN STATUS
			$scope.valorbtnStatus = vm.switches.btnStatus;
			console.log("btnStatus: " + $scope.valorbtnStatus);
			
			//apagar dados no retorno
			$scope.error = [];
			$scope.form.week = [];
			$scope.form.status = vm.switches.btnStatus;
			console.log(vm.switches.btnStatus);

			//Atribuir valores Coletados
			angular.forEach($scope.itemSemana, function(value, key) {
				if(value.active){
					$scope.form.week.push({
						"day_week" : value.name,
						"start" : value.start,
						"finish" : value.finish
					});
				}
			});

			//error recebe resposta de validate(arrayForm, ArrayErros);
			error = validate($scope.form, $scope.validateRules, {fullMessages: false});
			console.log("TAG ERROR ");
			console.log(error);
			
			//Caso não tenha erro
			if (!error) {
				var dados = $scope.form;
				//Chamar Serviço de Registro do Templo 
				$.post("http://dev.locker.cwsoftwares.com.br/appapi/Temple/Add", dados, function(result){
		            console.log(result);
		            if(result.status == "success"){
		            	alert('Cadastrado com sucesso.');
		            	// troca de tela manda pra listagem
		            	window.location.href = "#/templos";
		            } else {
		            	for(var field in result.error){
		            		$scope.error[field] = result.error[field];
		            	}
		            }
		        });

			} else {
				for(var field in error){
            		$scope.error[field] = error[field][0];
            	}
				console.log(error);
			}

		};
		


	}

})();