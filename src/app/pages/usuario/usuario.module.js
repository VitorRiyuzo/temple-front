/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.usuario', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('usuario', {
          url: '/usuario',
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'Usuario',
          sidebarMeta: {
            icon: '',
            order: 2,
          },
        })
        // .state('usuario.novousuario', {
        //   url: '/novousuario',
        //   templateUrl: 'app/pages/usuario/novousuario/novousuario.html',
        //   title: 'Novo Usuario',
        //   controller: 'novousuarioCtrl',
        //   controllerAs: 'vm',
        //   sidebarMeta: {
        //     order: 2,
        //   },
        // })
        .state('usuario.listausuario', {
          url: '/listausuario',
          templateUrl: 'app/pages/usuario/listausuario/usuario.html',
          title: 'Lista - Usuario',

          sidebarMeta: {
            order: 1,
          },
        });
  }
})();
