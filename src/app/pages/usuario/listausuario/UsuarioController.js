(function () {

  //'use strict';
    angular.module('BlurAdmin.pages.usuario')
        .controller('UsuarioController', UsuarioController)
        .service("TemploService",["$http", function ($http) {
            
            var service = this;
            //Serviço de desativar usuario;
            service.toggleStatus = function (mode, idTemplo, activeResp) {

                var req = {
                    method: 'POST',
                    url: 'http://dev.locker.cwsoftwares.com.br/appapi/User/AdminDesactivateUser',
                    headers: {
                        'Content-Type': undefined,
                        'Access-Token': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJTU3YrTnJrQ1lvWXNHdmFQUjJSNHJ2TXlHZGIwRDhwQUtyV2xzWVpsQjlBPSIsImlhdCI6MTUxNjEyMzM3MSwiaXNzIjoiZGV2LmxvY2tlci5jd3NvZnR3YXJlcy5jb20uYnIiLCJhdWQiOiJkZXYubG9ja2VyLmN3c29mdHdhcmVzLmNvbS5iciIsIm5iZiI6MTUxNjEyMzM3MSwiZXhwIjoxNTE4NzE1MzcxLCJkYXRhIjoiQUVIQkxEK2liNGFyOHNPeXlWU0FvWmEzdFlhYWh6VW5KT2JyVE1aQzVXTUFya3ZmWURKd2lBdEE4V3NvRVFyWElZcUgzTjBhaWc0N0hpenZIeWt0enc9PSJ9.agFLEy2ppmN2jIeLaPmogqI7d3CEfbcTBHoPj7PTcKqNqciWdlld0oxlXnwGWML29DBztaui3gPE3Hksxz95_g"
                    },
                }
                if (mode){
                    req.url = "http://dev.locker.cwsoftwares.com.br/appapi/User/AdminActivateUser"
                }
                var data = {
                    user_id: idTemplo,
                    status: activeResp,
                }
                var form_data = new FormData();
                for (var key in data) {
                    form_data.append(key, data[key]); 
                }
                req.data = form_data; 

                $http(req).then(function (result) {
                    alert(result.data.message);
                    console.log("Serviço de Mudar Status resposta: ");
                    console.log(result);
                    // callBack(result);

                }, function (result) {
                    alert("Não deu Certo :(");
                    console.log("Serviço de Mudar Status resposta: ");
                    console.log(result);
                });
            };

        }]);

    /** @ngInject */
    function UsuarioController($scope, $filter, $rootScope, NgMap, TemploService) {
        
        // function desactiveCallBack(result) {
        // }
        $(document).ready(function () {

            var activeResp = "";
            $scope.onToggle = function(idTemplo, atualStatus) {
                console.log(idTemplo + " " + atualStatus);
                 
                if (document.getElementById("input_toggle_" + idTemplo ).checked == true) {
                    activeResp = 1;
                    TemploService.toggleStatus(true, idTemplo, activeResp);
                    console.log(activeResp + " " + idTemplo);
                }else{
                    activeResp = 0;
                    TemploService.toggleStatus(false, idTemplo, activeResp);
                    console.log(activeResp + " " + idTemplo);
                }

            };
        });

        $scope.inituserdataTable = function(){    


            $("#map").hide(); 

            /* Datatable row highlight */
            $(document).ready(function() {

                $scope.grid = $('#datatable-row-highlight').DataTable({
                    "filter":   true,
                    "paging":   true,
                    "lengthChange": true,
                    "lengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
                    "processing": true,
                    "serverSide": true,
                    pageResize: true, 
                    "ordering": false,
                    "info":     false,
                    "responsive": true,
                    deferRender: true,
                    processing: true,
                    "language": {
                        "processing": "Processando. Aguarde a resposta..." //add a loading image,simply putting <img src="loader.gif" /> tag.
                    },
                    "ajax": {
                        "url":"http://dev.locker.cwsoftwares.com.br/appapi/User/UserTempleSearch",
                        "type": "POST",
                        data : function ( d ) {
                            return d;
                        },
                        dataFilter : function(data){

                            //resultado da requisição
                            var json = jQuery.parseJSON( data );
                            json.recordsTotal = json.pagination.total;
                            json.recordsFiltered = json.pagination.total;
                            return JSON.stringify( json ); // return JSON string

                        },
                        
                        dataSrc: function ( json ) {
                            //array que será impresso no grid
                            return json.data;
                        }
                    },
                    columns: [
                        //informações do grid

                        // { "data" : "id" },
                        // { "data" : "temple_name" },
                        // { "data" : "temple_name" },
                        // { "data" : "status" },
                        // { "data" : "temple_address" },
                        // { "data" : "temple_city" },
                        // { "data" : "temple_neighborhood" },
                        // { "data" : "temple_number" },
                        // { "data" : "temple_uf" },

                        { "data" : "id" },
                        { "data" : "name" },
                        { "data" : "created_at" },
                        { "data" : "username"},
                        { "data" : "phone_number"},
                        
                        { "render": function (data, type, full, meta) {
                            var dataUser = JSON.stringify(full);
                            return '<button id="btnShow" class="btn btn-primary" data-json=\'' + dataUser + '\' onclick="angular.element(\'#btnShow\').scope().openMaps(this)" >Ultima Localização</button> '+
                                '<label class="switch">' +
                                '<input id="input_toggle_' + full.id + '" onchange="angular.element(\'#input_toggle_' + full.id + '\').scope().onToggle(' + full.id + ',' + this.checked + ')" type="checkbox" ' + (full.active == 0 ? "" :"checked") +'>' +
                                '<span class="slider round"></span>' +
                                '</label>';
                        } },
                    ],
                    /* MODIFICANDO OS PARAMETROS DE FILTRO E PAGINACAO  */
                    fnServerParams: function ( aoData ) {   
                        
                        //mandar parametros paraa requisição
                        aoData.page = ((aoData.start + aoData.length) / aoData.length) -1;
                        aoData['page-size'] = aoData.length;
                        aoData.keyword = aoData.search.value;
                        
                    },
                    "rowCallback": function( row, data ) {},
                    /*"columnDefs": [ {
                        "targets": -1,
                        "data": null,
                        "defaultContent": ''
                    } ]*/
                });
                
                /*
                $('#datatable-row-highlight tbody').on( 'click', 'tr', function () {
                    var id = $(this).find('td:first-child').text();
                    $state.go("painel.userEditProfile",{"id":id});
                } );
                */
                
            });
        };

        $scope.inituserdataTable();

        // FUNCTION OPEN GOOGLE MAPS
        $scope.openMaps = function (dataUser) {
            $rootScope.dataUser = '';
            $rootScope.dataUser = $(dataUser).data("json");
            console.log('$rootScope.dataUser', $rootScope.dataUser);
            $("#map").show();
        }

        // $scope.maps = function(evt){
        //     var json = $(evt).data("json");
        //     $rootScope.lat = parseFloat(json.lat);
        //     $rootScope.lon = parseFloat(json.lon);      
        //     console.log($rootScope.lat);       
        //     $("#dialog").dialog({
        //         modal: true,
        //         title: "Google Map",
        //         width: 600,
        //         hright: 450, 
        //         buttons: {
        //             Fechar: function () {
        //                 $(this).dialog('Fechar');
        //             }
        //         },
        //         open: function () {
        //             var mapOptions = {
        //                 center: new google.maps.LatLng(-23.5505, -46.6333),
        //                 zoom: 10,                    
        //                 mapTypeId: google.maps.MapTypeId.ROADMAP
        //             };
        //             var mylatlon = {
        //                 lat: $rootScope.lat,
        //                 lng: $rootScope.lon
        //             };
        //             var map = new google.maps.Map($("#dvMap")[0], mapOptions);
        //             var contentString = '<div id="content">'+
        //             '<div id="siteNotice">'+
        //             '</div>'+
        //             '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
        //             '<div id="bodyContent">'+
        //             '<p><b></b>, also referred to as <b>Ayers Rock</b>, is a large ' +
        //             'sandstone rock formation in the southern part of the '+
        //             'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
        //             'south west of the nearest large town, Alice Springs; 450&#160;km '+
        //             '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
        //             'features of the Uluru - Kata Tjuta National Park. Uluru is '+
        //             'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
        //             'Aboriginal people of the area. It has many springs, waterholes, '+
        //             'rock caves and ancient paintings. Uluru is listed as a World '+
        //             'Heritage Site.</p>'+
        //             '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
        //             'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
        //             '(last visited June 22, 2009).</p>'+
        //             '</div>'+
        //             '</div>';
        //             var infowindow = new google.maps.InfoWindow({
        //             content: contentString
        //             });
        //             var marker = new google.maps.Marker({                    
        //             position:mylatlon,
        //             title:""
        //             });
        //             marker.setMap(map);
        //             marker.addListener('click', function() {
        //                 infowindow.open(map, marker);
        //             });

        //         }
        //     });         
        // }
    }

})();