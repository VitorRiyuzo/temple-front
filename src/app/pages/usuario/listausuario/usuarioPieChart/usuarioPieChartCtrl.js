/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.usuario')
      .controller('usuarioPieChartCtrl', usuarioPieChartCtrl);

  /** @ngInject */
  function usuarioPieChartCtrl($scope, $timeout, baConfig, baUtil) {
      $scope.printTotal = function(data){
        $(".description-stats").html(data.total);
      };

      $.ajax({
        url: "http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", 
        success: function(data){
          $scope.printTotal(data);
        }
      });

      var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
      $scope.charts = [{
        color: pieColor,
        description: 'Usuarios',
        stats: $scope.totalUsuarios,
        icon: 'church',
      }];





    function getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }

    function loadPieCharts() {
      $('.chart').each(function () {
        var chart = $(this);
        chart.easyPieChart({
          easing: 'easeOutBounce',
          onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          },
          barColor: chart.attr('rel'),
          trackColor: 'rgba(0,0,0,0)',
          size: 84,
          scaleLength: 0,
          animation: 2000,
          lineWidth: 9,
          lineCap: 'round',
        });
      });

      $('.refresh-data').on('click', function () {
        updatePieCharts();
      });
    }

    function updatePieCharts() {
      
          $scope.usuariosAtivasPorcentagem = $.get("http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", function(data){
          $scope.usuariosAtivasPorcentagemValor = parseInt(data.data);
          $('.pie-charts .chart').each(function(index, chart) {
            $(chart).data('easyPieChart').update($scope.usuariosAtivasPorcentagemValor);
          });
        });
    }

    $timeout(function () {
      loadPieCharts();
      updatePieCharts();
    }, 1000);
  }
})();


// (function () {
//   'use strict';

//   angular.module('BlurAdmin.pages.usuario')
//       .controller('usuarioPieChartCtrl', usuarioPieChartCtrl);

//   /** @ngInject */
//   function usuarioPieChartCtrl($scope, $timeout, baConfig, baUtil) {
//     var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
//     $scope.charts = [{
//       color: pieColor,
//       description: 'Usuários',
//       stats: '2000',
//       icon: 'person',
//     }
//     ];

//     function getRandomArbitrary(min, max) {
//       return Math.random() * (max - min) + min;
//     }

//     function loadPieCharts() {
//       $('.chart').each(function () {
//         var chart = $(this);
//         chart.easyPieChart({
//           easing: 'easeOutBounce',
//           onStep: function (from, to, percent) {
//             $(this.el).find('.percent').text(Math.round(percent));
//           },
//           barColor: chart.attr('rel'),
//           trackColor: 'rgba(0,0,0,0)',
//           size: 84,
//           scaleLength: 0,
//           animation: 2000,
//           lineWidth: 9,
//           lineCap: 'round',
//         });
//       });

//       $('.refresh-data').on('click', function () {
//         updatePieCharts();
//       });
//     }

//     function updatePieCharts() {
//       $('.pie-charts .chart').each(function(index, chart) {
//         $(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
//       });
//     }

//     $timeout(function () {
//       loadPieCharts();
//       updatePieCharts();
//     }, 1000);
//   }
// })();