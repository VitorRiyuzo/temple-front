/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.usuario')
      .directive('usuarioPieChart', usuarioPieChart);

  /** @ngInject */
  function usuarioPieChart() {
    return {
      restrict: 'E',
      controller: 'usuarioPieChartCtrl',
      templateUrl: 'app/pages/usuario/listausuario/usuarioPieChart/usuarioPieChart.html'
    };
  }
})();