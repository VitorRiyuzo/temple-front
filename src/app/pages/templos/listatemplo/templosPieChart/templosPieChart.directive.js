/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.templos')
      .directive('templosPieChart', templosPieChart);

  /** @ngInject */
  function templosPieChart() {
    return {
      restrict: 'E',
      controller: 'templosPieChartCtrl',
      templateUrl: 'app/pages/templos/listatemplo/templosPieChart/templosPieChart.html'
    };
  }
})();