(function () {
 // 'use strict';

  angular.module('BlurAdmin.pages.templos')
      .controller('novotemploCtrl', novotemploCtrl);

	/** @ngInject */
	function novotemploCtrl($scope, $state, $stateParams) {
		
		var vm = this;
		vm.switches = {
	      btnStatus : true,
		};
		//Estados
		$scope.estados = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"];
		$scope.choiceStatus = ["active", "inactive"];
		//Array de models do formulario de registro de novo templo.
		$scope.form = {
			temple_name: "",
			temple_neighborhood: "",
			temple_address: "",
			temple_number: "",
			temple_city: "",
			temple_uf: "",
			lat: "",
			lon: "",
			created_at: "",
			status: "",
			// week : {
			// 	day_week : "",
			// 	start : "",
			// 	finish : ""
			// }
		};

		//Objeto Aninhada Semana
		$scope.itemSemana = [
			{ "title": "Domingo", "name": "domingo", "active": false, "start": "00:00", "finish": "00:00"},
			{ "title": "Segunda", "name": "segunda", "active": false, "start": "00:00", "finish": "00:00"},
			{ "title": "Terça",   "name": "terca",   "active": false, "start": "00:00", "finish": "00:00"},
			{ "title": "Quarta",  "name": "quarta",  "active": false, "start": "00:00", "finish": "00:00"},
			{ "title": "Quinta",  "name": "quinta",  "active": false, "start": "00:00", "finish": "00:00"},
			{ "title": "Sexta",   "name": "sexta",   "active": false, "start": "00:00", "finish": "00:00"},
			{ "title": "Sábado",  "name": "sabado",  "active": false, "start": "00:00", "finish": "00:00"},
		];

		//value  = valor do campo
		//options parametros
		//key = Chave do obejeto
		$scope.validTime = function ( value, options, key, attributes ) {
			if (Array.isArray( value ) && value.length > 0) {
				for ( var i in value ) {
					var row = value[i];
					console.log( row );

					if (['segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sabado', 'domingo'].indexOf(row.day_week) == -1) {
						console.log("Dia da semana inválido");
						return "Dia da semana inválido";
					}
					if (row.start == "" || row.finish == "") {
						console.log("Preencha o horario de funcionamento");
						return "Preencha o horario de funcionamento";
					}

					console.log(Date.parse('2018-01-01 ' + row.start), Date.parse('2018-01-01 ' + row.finish));

					if (Date.parse('2018-01-01 ' + row.start) >= Date.parse('2018-01-01 ' + row.finish)) {
						console.log("Horario de inicio tem que ser menor que horario de fim");
						return "Horario de Inicio tem que ser menor que horario de Termino";
					}
				}
				return null;
			} else {
				return options.message;
			}
		};

		validate.validators.timeValid = $scope.validTime;

		$scope.validateRules = {
			temple_name: { presence: { message: "Informe o nome", allowEmpty: false } },
			temple_address: { presence: { message: "Informe o endereço", allowEmpty: false } },
			temple_number: { presence: { message: "Informe o numero", allowEmpty: false } },
			temple_neighborhood: { presence: { message: "Informe o bairro", allowEmpty: false } },
			temple_uf: { presence: { message: "Informe o estado", allowEmpty: false } },
			week: { timeValid: { message: " Informe ao menos um horario de funcionamento" } },
			lat: { presence: { message: "Informe Latitude.", allowEmpty: false } },
			lon: { presence: { message: "Informe Longitude.", allowEmpty: false } },
			temple_city: {presence: {message: "Informe o cidade",allowEmpty: false} },
		};

		
		// Consultar modo de edição
		var editMode = false;
		$stateParams.id > 0 ? editMode = true : editMode = false;
		
		console.log("Edit Mode Status: " + editMode);


		// Popular area de edição.
		if (editMode){
			console.log("Entrou no edit");

			var dados = {id: $stateParams.id}; 

			$.post("http://dev.locker.cwsoftwares.com.br/appapi/Temple/FindOne", dados, function (result) {
				if (result.status == "success") {

					console.log("Result");
					console.log(result); 

					angular.forEach( $scope.form, function ( value, key ) {
						$scope.form[key] = result.data[key];
					});

					angular.forEach (result.data.week, function (v, k) {
						angular.forEach($scope.itemSemana, function (v2, k2){
							if (v.day_week == v2.name){
								$scope.itemSemana[k2].active = true;
								$scope.itemSemana[k2].start = v.start;
								$scope.itemSemana[k2].finish = v.finish;
							}
						});
					});

					console.log("Item Semana");
					console.log($scope.itemSemana);

				} else {
					for (var field in result.error) {
						$scope.error[field] = result.error[field];
					}
				}
				$scope.$apply();
			});
		}

		// Enviar Formulário
		$scope.onSubmit = function () {

			console.log("scope.onSubmit");

			// Apagar dados no retorno
			$scope.error = [];
			$scope.form.week = [];

			// Atribuir valores Coletados
			angular.forEach($scope.itemSemana, function (value, key) {
				if (value.active) {
					$scope.form.week.push({
						"day_week": value.name,
						"start": value.start,
						"finish": value.finish
					});
				}
			});

			console.log($scope.form);

			// Error recebe resposta de validate(arrayForm, ArrayErros);
			error = validate($scope.form, $scope.validateRules, { fullMessages: false });

			console.log("TAG ERROR");
			console.log(error);

			// Caso não tenha erro

			if (!error) {
				$scope.form.id = $stateParams.id;

				for (field in $scope.form.week){
					$scope.form.week[field].temple_id = $stateParams.id
				}

				var dados = $scope.form;

				console.log(dados);
				
				if (editMode) {
					console.log("Entrou serviço de EDITAR");
					// Chamar Serviço de Editar Templo

					$.post("http://dev.locker.cwsoftwares.com.br/appapi/Temple/edit", dados, function (result) {
						console.log(result);
						if (result.status == "success") {
							alert('Templo editado com sucesso.');
							// Troca de tela manda pra listagem
							window.location.href = "#/templos/listatemplo";
						} else {
							for (var field in result.error) {
								$scope.error[field] = result.error[field];
							}
						}
					});
				} else {
					console.log("Entrou serviço de REGISTRO");
					// Chamar Serviço de Registro novo Templo 
					$.post("http://dev.locker.cwsoftwares.com.br/appapi/Temple/Add", dados, function (result) {
						console.log(result);
						if (result.status == "success") {
							alert('Templo Cadastrado com sucesso.');

							// troca de tela manda pra listagem
							window.location.href = "#/templos/listatemplo";
						} else {
							for (var field in result.error) {
								$scope.error[field] = result.error[field];
							}
						}
					});

				}
			} else {
				for (var field in error) {
					$scope.error[field] = error[field][0];
				}
				console.log(error);
			}
		};
	}

})();