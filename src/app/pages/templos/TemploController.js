(function () {
 // 'use strict';

  angular.module('BlurAdmin.pages.templos')
      .controller('TemploController', TemploController);

  /** @ngInject */
  function TemploController($scope, $filter) {

    $scope.initdataTable = function(){
        /* Datatable row highlight */
        $(document).ready(function() {
            $scope.grid = $('#datatable-row-highlight').DataTable({
                "filter":   true,
                "paging":    true,
                "lengthChange": true,
                "lengthMenu": [[10, 25, 50, 100, 1000], [10, 25, 50, 100, 1000]],
                "processing": true,
                "serverSide": true,
                pageResize: true, 
                "ordering": false,
                "info":     false,
                "responsive": true,
                deferRender: true,
                processing: true,
                "language": {
                    "processing": "Processando. Aguarde a resposta..." //add a loading image,simply putting <img src="loader.gif" /> tag.
                },
                "ajax": {
                    "url":"http://dev.locker.cwsoftwares.com.br/appapi/temple/search",
                    "type": "POST",
                    data : function (d) {
                        return d;
                    },
                    dataFilter : function(data){
                        //resultado da requisição
                        var json = jQuery.parseJSON( data );
                        console.log("json", json);
                        json.recordsTotal = json.pagination.total;
                        json.recordsFiltered = json.pagination.total;
                        return JSON.stringify( json ); // return JSON string
                    },
                    
                    dataSrc: function ( json ) {
                        console.log(json);
                        
                        //array que será impresso no grid
                        return json.data;
                    }
                },
                columns: [

                    //informações do grid
                    { "data": "id" },
                    { "data" : "temple_name" },
                    { "data" : "created_at" },
                    { "data": "status" },
                    { "data" : "temple_address" },
                    { "data" : "temple_city" },
                    { "data" : "temple_neighborhood" },
                    { "data" : "temple_number" },
                    { "data" : "temple_uf" },
                    { "render": function(data, type, full, meta){
                        return '<button class="btn btn-primary btn_edit" onclick="angular.element(\'[ng-controller=TemploController]\').scope().onEdit(' + full.id + ')">Editar</button>';
                    } },
                ],
                /* MODIFICANDO OS PARAMETROS DE FILTRO E PAGINACAO  */
                fnServerParams: function ( aoData ) {
                    //mandar parametros paraa requisição
                    console.log(aoData);
                    aoData.page = ((aoData.start + aoData.length) / aoData.length) -1;
                    aoData['page-size'] = aoData.length;
                    aoData.keyword = "";
                    
                },
                "rowCallback": function( row, data ) {},
            });
        });
    };
    $scope.initdataTable();

    $scope.onEdit = function (id) {
        $rootScope.messageId = id;
        $state.go("notificacoes.listanotificacoes", { id: id });
    };

  }

})();