/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.templos', ['ui.toggle'])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('templos', {
          url: '/templos',
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'Templos',
          sidebarMeta: {
            icon: 'ion-compose',
            order: 1,
          },
        })
        .state('templos.novotemplo', {
          url: '/novotemplo/:id',
          templateUrl: 'app/pages/templos/novotemplo/novotemplo.html',
          title: 'Novo Templo',
          controllerAs: 'vm',
          sidebarMeta: {
            order: 2,
          },
        })
        .state('templos.listatemplo', {
          url: '/listatemplo',
          templateUrl: 'app/pages/templos/listatemplo/templos.html',
          title: 'Lista - Templos',
          sidebarMeta: {
            order: 1,
          },
        });
        
  }
})();
