/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.notificacoes', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('notificacoes', {
        url: '/notificacoes',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Notificacoes',
        sidebarMeta: {
          icon: 'ion-compose',
          order: 1,
        },
      })
      .state('notificacoes.novonotificacoes', {
        url: '/novo/:id',
        templateUrl: 'app/pages/notificacoes/novonotificacoes/novonotificacoes.html',
        title: 'Nova - Notificação',
        //controller: 'NovoNotificacoesCtrl',
        sidebarMeta: {
          order: 2,
        },
      })
      .state('notificacoes.listanotificacoes', {
        url: '/lista',
        templateUrl: 'app/pages/notificacoes/listanotificacoes/notificacoes.html',
        title: 'Lista - Notificações',
        //controller: 'NotificacoesController',
        sidebarMeta: {
          order: 1,
        },
      });
  }
})();
