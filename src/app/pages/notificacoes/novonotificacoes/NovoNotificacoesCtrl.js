(function () {
    //'use strict';

    angular.module('BlurAdmin.pages.notificacoes')
        .controller('NovoNotificacoesCtrl', NovoNotificacoesCtrl);

    /** @ngInject */
    function NovoNotificacoesCtrl($scope, $rootScope, $state, $stateParams) {

        console.log("meus parametros ", $stateParams);//{id:4545}
            
        $scope.error = {};

        var msgId = $stateParams.id;
        var dados = { id: msgId };

        $scope.form = {
            message : "Carregando...",
        };
        
        $.post("http://dev.locker.cwsoftwares.com.br/appapi/TempleNotify/FindOne", dados , function (result) {
            console.log($scope.form);
            if (result.status == "success") {
                
                $scope.form.message = result.data.message;

            } else {
                console.log(":( Não foi possivel carregar mensagem atual");
            }
        });

        $scope.onSubmit = function () {
            dados = {
                id: msgId,
                message: $scope.form.message,
            };
            $.post("http://dev.locker.cwsoftwares.com.br/appapi/TempleNotify/Edit", dados, function (result) {

                if (result.status == "success") {
                    alert("Mensagem alterada! " + result.data.message);
                    $state.go("notificacoes.listanotificacoes");
                } else {
                    alert(":(", "Não foi possivel alterar mensagem atual, porfavor tente mais tarde.");
                }
            });
        }
    }

})();