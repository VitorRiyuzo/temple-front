/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.notificacoes')
      .directive('notificacoesPieChart', notificacoesPieChart);

  /** @ngInject */
  function notificacoesPieChart() {
    return {
      restrict: 'E',
      controller: 'notificacoesPieChartCtrl',
      templateUrl: 'app/pages/notificacoes/listanotificacoes/notificacoesPieChart/notificacoesPieChart.html'
    };
  }
})();