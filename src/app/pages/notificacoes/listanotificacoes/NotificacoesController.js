(function () {
  //'use strict';

  angular.module('BlurAdmin.pages.notificacoes')
      .controller('NotificacoesController', NotificacoesController);

    /** @ngInject */
    function NotificacoesController($scope, $filter, $rootScope, $state, $stateParams, $timeout, $interval) {

        $scope.initNotificacoesDataTable = function(){

            /* Datatable row highlight */
            $(document).ready(function() {
                
                // DataTable
                $scope.grid = $('#datatable-row-highlight').DataTable({
                    "filter": false,
                    "paging": true,
                    "lengthChange": true,
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "processing": true,
                    "serverSide": true,
                    pageResize: true, 
                    "ordering": false,
                    "info": false,
                    "responsive": true,
                    deferRender: true,
                    processing: true,
                    "language": {
                        "processing" : "Processando. Aguarde a resposta..." //add a loading image,simply putting <img src="loader.gif" /> tag.
                    },
                    "ajax": {
                        "url" : "http://dev.locker.cwsoftwares.com.br/appapi/TempleNotify/Search",
                        "type" : "POST",
                        data : function ( d ) {
                            return d;   
                        },
                        dataFilter : function(data){
                            //Resultado da requisição
                            var json = jQuery.parseJSON( data );
                            console.log("json", json);

                            json.recordsTotal = json.pagination.total;
                            json.recordsFiltered = json.pagination.total;
                            return JSON.stringify( json ); // return JSON string
                        },
                        dataSrc: function ( json ) {
                            console.log(json);
                            //Array que será impresso no grid
                            return json.data;
                        }
                    },
                    columns: [
                        { "data": "id" },
                        { "data": "message" },
                        { "render": function(data, type, full, meta){ 
                            return '<button class="btn btn-primary btn_edit" onclick="angular.element(\'[ng-controller=NotificacoesController]\').scope().onEdit(' + full.id + ')">Editar</button>';                    
                        } },
                    ],
                    /* MODIFICANDO OS PARAMETROS DE FILTRO E PAGINACAO  */
                    fnServerParams: function ( aoData ) {
                        //mandar parametros paraa requisição
                        aoData.currentPage = ((aoData.start + aoData.length) / aoData.length) -1;
                        aoData['page-size'] = aoData.length;
                        aoData.keyword = "";
                        
                    },
                    "rowCallback": function( row, data ) {},
                });
            });
        };
        console.log("onedit");
        $scope.onEdit = function (id) {
            $rootScope.messageId = id;
            $state.go("notificacoes.novonotificacoes", { id : id });
        };
        $interval(function() {
            $scope.onEdit = function (id) {
                $rootScope.messageId = id;
                $state.go("notificacoes.novonotificacoes",{ id : id });
            };
        }, 500);
         $scope.initNotificacoesDataTable();
        
        
    }

})();