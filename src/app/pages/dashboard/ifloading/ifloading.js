(function() {

    angular
    .module('BlurAdmin.pages.dashboard')
    .directive('ifLoading', ifLoading);

    ifLoading.$injector = ['$http'];

    function ifLoading($http){

        return {
            restrict: 'A',
            templateUrl: 'app/pages/dashboard/dashboardMap/dashboardMap.html',
            link: function(scope, elem) {
                scope.isLoading = isLoading;

                scope.$watch(scope.isLoading, toggleElement);

                ////////

                function toggleElement(loading) {

                  if (loading) {
                    elem[0].style.display = "block";
                  } else {
                    elem[0].style.display = "none";
                  }
                }

                function isLoading() {
                  return $http.pendingRequests.length > 0;
                }
            }
        }
    };

})();
