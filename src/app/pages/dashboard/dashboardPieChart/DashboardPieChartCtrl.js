/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      .controller('DashboardPieChartCtrl', DashboardPieChartCtrl);

  /** @ngInject */
  function DashboardPieChartCtrl($scope, $timeout, baConfig, baUtil) {
   
    // $.ajax({
    //   url: "http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", 
    //   success: function(data){
    //     $scope.printTotalUser(data);
    //   }
    // });
    // $.ajax({
    //   url: "http://dev.locker.cwsoftwares.com.br/appapi/Temple/TotalActives/", 
    //   success: function(data){
    //     $scope.printTotalTemple(data);
    //   }
    // });

    // $.ajax({
    //   url: "http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", 
    //   success: function(data){
    //     $scope.printTotal(data);
    //   }
    // });
    // $.ajax({
    //   url: "http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", 
    //   success: function(data){
    //     $scope.printTotal(data);
    //   }
    // });

    var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
    $scope.charts = [{
      color: pieColor,
      description: 'Usuários',
      stats: $scope.totalUsuarios,
      icon: 'person',
    }, {
      color: pieColor,
      description: 'Templos',
      stats: $scope.totalTemples,
      icon: 'face',
    // }, {
    //   color: pieColor,
    //   description: 'Notificações',
    //   stats: '178,391',
    //   icon: 'face',
    // }, {
    //   color: pieColor,
    //   description: 'Returned',
    //   stats: '32,592',
    //   icon: 'refresh',
     }
    ];

   

    $scope.init = function() {
      var icon = "person";
      var icon2 = "face";
      var nome = "Usuários";
      var nome2 = "Templos";
      $.get("http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", function(resp){ $scope.onComplete(resp,0, icon, nome )});
      $.get("http://dev.locker.cwsoftwares.com.br/appapi/Temple/TotalActives/", function(resp){ $scope.onComplete(resp,1, icon2, nome2)});
      // $.get("http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", function(resp){ $scope.onComplete(resp,2)});
      // $.get("http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", function(resp){ $scope.onComplete(resp,4)});
      // $.get("http://dev.locker.cwsoftwares.com.br/appapi/User/TotalActives/", function(resp){ $scope.onComplete(resp,5)});
    }

    $scope.onComplete = function(data,idx,icon,nome){
      console.log(icon);
      $scope.charts[idx] = {
          color: pieColor,
          description: nome,
          stats: parseInt(data.total),
          percent: parseInt(data.data),
          icon: icon,
      };
      $timeout(function () {
        $scope.applyChart();
      }, 5000);
    };

    $scope.applyChart = function (){
      console.log('13200');
      $('.chart').each(function () {
        var chart = $(this);
        chart.easyPieChart({
          //easing: 'easeOutBounce',
          onStep: function (from, to, percent) {
            console.log()
            $(this.el).find('.percent').text(Math.round(percent));
          },
          barColor: chart.attr('rel'),
          trackColor: 'rgba(0,0,0,0)',
          size: 84,
          scaleLength: 0,
          //animation: 2000,
          lineWidth: 9,
          lineCap: 'round',
        });
      });
      /*
      $('.refresh-data').on('click', function () {
        $('.chart').each(function(index, chart) {
          $(chart).data('easyPieChart').update(parseInt($(chart).data('percent')));
        })
      });
      */
    }

   // function updatePieCharts() {
      
       
      
    //     $scope.igrejasAtivasPorcentagem = $.get("http://dev.locker.cwsoftwares.com.br/appapi/Temple/TotalActives/", function(dataTemple){
    //     $scope.igrejasAtivasPorcentagemValor = parseInt(dataTemple.data);
    //     $('.pie-charts .chart').each(function(index, chart) {
    //       $(chart).data('easyPieChart').update($scope.igrejasAtivasPorcentagemValor);
    //     });
    // });
//}//
    $(document).ready(function(){
      $scope.init();
    });

  }
})();