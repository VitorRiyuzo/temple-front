(function () {
    'use strict';
    angular.module('BlurAdmin.pages.dashboard')
        .controller('DashboardController', DashboardController);
    /** @ngInject */


    function DashboardController($scope, $filter, $rootScope, $http) {
        // Função de Logout
        $scope.logOut = function () {
            var token = window.localStorage.getItem('adminToken');
            var req = {
                method: 'POST',
                url: 'http://dev.locker.cwsoftwares.com.br/appapi/User/Logout',
                headers: {'Access-Token': token }
            };
            $http(req).then(function (result) {
                console.log(result);
                localStorage.removeItem('adminToken');
                location.href = window.location.origin + "/auth.html";
            }, function (result) {
                console.log(result);
            });

        };

        $scope.initMap = function () {
            $(document).ready(function () {
                var locations = [];
                var result;
                var marker = {};
                var result2;
                var igreja = 'https://i.imgur.com/ZtYC6UT.png';
                var pessoa = '';
                $.ajax({
                    url: "http://dev.locker.cwsoftwares.com.br/appapi/User/totalUserWithLatAndLon",
                    data: { "page-size": '1000' },
                    success: function (data) {
                        result = data.data;
                        for (var row in result) {
                            if (typeof result[row].lat != undefined && typeof result[row].lon != undefined) {
                                locations.push(["usuario", result[row]]);
                            }
                        };
                        $.ajax({
                            url: "http://dev.locker.cwsoftwares.com.br/appapi/Temple/Search",
                            data: { "page-size": '100' },
                            success: function (data2) {

                                result2 = data2.data;
                                for (var row in result2) {
                                    if (result2[row].lat != undefined && result2[row].lon != undefined) {
                                        locations.push(["igreja", result2[row]]);
                                    }
                                }
                                //CRAINDO MAPA
                                var map = new google.maps.Map(document.getElementById('map'), {
                                    zoom: 10,
                                    center: new google.maps.LatLng(-23.5505, -46.6333),
                                });
                                var bounds = new google.maps.LatLngBounds();
                                var infowindow = new google.maps.InfoWindow();

                                //CRIAR UM PIM PARA CADA LOCATION
                                for (var i in locations) {
                                    var location = locations[i];
                                    var position = new google.maps.LatLng(parseFloat(location[1].lat), parseFloat(location[1].lon));
                                    bounds.extend(position);
                                    if (location[0] == "igreja") {
                                        var marker = new google.maps.Marker({
                                            position: position,
                                            map: map,
                                            icon: igreja,
                                            info: $scope.infoTempleRender(location[1])
                                        });

                                    } else {
                                        var marker = new google.maps.Marker({
                                            position: position,
                                            info: $scope.infoUserRender(location[1]),
                                            //label: location[1].username,
                                            map: map,
                                        });
                                    }
                                    //Adicionar clique ao marcador
                                    marker.addListener('click', function () {
                                        infowindow.setContent(this.info);
                                        infowindow.open(map, this);
                                    });
                                }
                            }
                        });
                    }
                });
            });
        }

        $scope.infoTempleRender = function (data) {
            console.log(data);

            var $ul = $("<ul>");
            String.prototype.capitalize = function () {
                return this.charAt(0).toUpperCase() + this.slice(1);
            }

            for(var i in data.week){
                $ul.append(
                    $("<li>").text(data.week[i].day_week.capitalize() + " " + data.week[i].start.substr(0, 5) + " às " + data.week[i].finish.substr(0, 5)).css("list-style-type", "none")
                ).css("padding", "0");
            }


            return $("<div>").append(
                $("<h3>").text(data.temple_name)
            ).append(
                    $("<br>")
                ).append(
                    $("<b>").text("Endereço:")
                ).append(
                    $("<p>").text(data.temple_address + " " + data.temple_number + ", " + data.temple_neighborhood + " - " + data.temple_uf)
                ).append(
                    $("<b>").text("Cultos")
                ).append(
                    $("<br>")
                ).append(
                    $ul
                )
                .html();
        }

        $scope.infoUserRender = function (data) {
            console.log(data);
            /*
            <div class="box-map">
                <div class="btn-close-map">Fechar</div>
                <ng-map style="height: 100%;" zoom="12" center="[74.18, -74.18]">
                    <marker id="foo" position="-23.59, -46.69" on-click="map.showInfoWindow('bar')">
                        <info-window id="bar" visible-on-marker="foo">
                            <div ng-non-bindable>
                                <div id="siteNotice"></div>
                                <div id="bodyContent">
                                    <div class="card-info-user">
                                        <div class="box-avatar">
                                            <img class="img-avatar" ng-src="https://pbs.twimg.com/profile_images/642430588343971840/Wg86hZKu_400x400.jpg">
							</div>
                                            <p><strong>Nome:</strong> {{ dataUser.username }}</p>
                                            <p><strong>Email:</strong> {{ dataUser.email }}</p>
                                            <p><strong>Cel:</strong> {{ dataUser.phone_number }}</p>
                                        </div>
                                    </div>
                                </div>
                        </info-window>
                    </marker>
                </ng-map>
            </div>
            */
            return $("<div>").append(
                $("<div>").append(
                    $("<div>").append(
                        $("<img>").attr({ "src": data.avatar }).addClass("img-avatar")
                    ).addClass("box-avatar")
                ).append(
                    $("<h3>").text(data.name).css("text-transform", "uppercase")
                ).append(
                    $("<p>").html("<b>Email:</b> " + data.email)
                ).append(
                    $("<p>").html("<b>Cel:</b> " + data.phone_number)
                ).addClass("box-map")
            )
            .html()
        }
        $scope.initMap();
    }
})();