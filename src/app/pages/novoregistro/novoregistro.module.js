/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.novoregistro', ['ui.select', 'ngSanitize', 'ngMask'])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('novoregistro', {
          url: '/novoregistro',
          template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'Novos Registros',
          sidebarMeta: {
            icon: 'ion-compose',
            order: 250,
          },
        })
        .state('novoregistro.inputs', {
          url: '/inputs',
          templateUrl: 'app/pages/novoregistro/inputs/inputs.html',
          title: 'Novo Templo',
          sidebarMeta: {
            order: 0,
          },
        })
        .state('novoregistro.layouts', {
          url: '/layouts',
          templateUrl: 'app/pages/novoregistro/layouts/layouts.html',
          title: 'Form Layouts',
          sidebarMeta: {
            order: 100,
          },
        })
        .state('novoregistro.wizard',
        {
          url: '/wizard',
          templateUrl: 'app/pages/novoregistro/wizard/wizard.html',
          controller: 'WizardCtrl',
          controllerAs: 'vm',
          title: 'Form Wizard',
          sidebarMeta: {
            order: 200,
          },
        });
  }
})();
